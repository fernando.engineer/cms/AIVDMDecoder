package engineer.fernando.ais.decoder.exceptions;

public class SixbitsExhaustedException extends Exception
{
	public SixbitsExhaustedException( String str )
	{
		super(str);
	}
}