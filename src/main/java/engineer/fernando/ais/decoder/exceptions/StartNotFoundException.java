package engineer.fernando.ais.decoder.exceptions;

public class StartNotFoundException extends Exception
{
	public StartNotFoundException( String str )
	{
		super(str);
	}
}

