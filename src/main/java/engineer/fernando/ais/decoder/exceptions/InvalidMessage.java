/*
 * InvalidMessage
 */

package engineer.fernando.ais.decoder.exceptions;

@SuppressWarnings("serial")
public class InvalidMessage extends RuntimeException {

	public InvalidMessage(String message) {
		super(message);
	}
	
}