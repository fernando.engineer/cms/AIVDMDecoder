package engineer.fernando.ais.decoder.exceptions;

public class StringSupplierException extends RuntimeException {

    public StringSupplierException(String message) {
        super(message);
    }
}