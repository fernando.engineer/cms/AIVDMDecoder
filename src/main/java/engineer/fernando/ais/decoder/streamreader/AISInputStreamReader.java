/*
 * AISInputStreamReader
 */

package engineer.fernando.ais.decoder.streamreader;

import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.nmea.NMEAMessageHandler;

import java.io.InputStream;
import java.util.function.Consumer;

public class AISInputStreamReader {

    public AISInputStreamReader(InputStream inputStream, Consumer<? super AISMessage> aisMessageConsumer) {
        this.nmeaMessageHandler = new NMEAMessageHandler("SRC", aisMessageConsumer);
        this.nmeaMessageInputStreamReader = new NMEAMessageInputStreamReader(inputStream, this.nmeaMessageHandler::accept);
    }

    public void run() {
        this.nmeaMessageInputStreamReader.run();
    }

    private final NMEAMessageHandler nmeaMessageHandler;
	private final NMEAMessageInputStreamReader nmeaMessageInputStreamReader;
}