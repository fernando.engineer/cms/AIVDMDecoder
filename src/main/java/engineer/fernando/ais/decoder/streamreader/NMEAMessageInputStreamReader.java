/*
 * NMEAMessageInputStreamReader
 */

package engineer.fernando.ais.decoder.streamreader;

import engineer.fernando.ais.decoder.exceptions.InvalidMessage;
import engineer.fernando.ais.decoder.exceptions.NMEAParseException;
import engineer.fernando.ais.decoder.exceptions.StringSupplierException;
import engineer.fernando.ais.decoder.exceptions.UnsupportedNMEAMessageType;
import engineer.fernando.ais.decoder.util.LoggerTools;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;


public class NMEAMessageInputStreamReader {

	private static final Logger LOG = LogManager.getLogger(NMEAMessageInputStreamReader.class);

	public NMEAMessageInputStreamReader(InputStream inputStream, Consumer<? super NMEAMessage> nmeaMessageHandler) {
		this.nmeaMessageHandler = nmeaMessageHandler;

		var reader = new InputStreamReader(inputStream, Charset.defaultCharset());
		var bufferedReader = new BufferedReader(reader);
		this.stringSupplier = () -> {
			try {
				return bufferedReader.readLine();
			} catch (IOException e) {
				throw new StringSupplierException(e.getMessage());
			}
		};
	}


	public void run() {
		LoggerTools.logInfo(LOG,"NMEAMessageInputStreamReader running.");
		String string;
		while ((string = stringSupplier.get()) != null) {
			if (Boolean.TRUE.equals(isStopRequested()))
				break;

			try {
				var nmea = NMEAMessage.fromString(string);
				nmeaMessageHandler.accept(nmea);
				LoggerTools.logDebug(LOG,new StringBuilder().append("Received: ").append(nmea).toString());
			} catch (InvalidMessage invalidMessageException) {
				LoggerTools.logError(LOG,new StringBuilder().append("Received invalid AIS message: ").append(string).toString());
			} catch (UnsupportedNMEAMessageType unsupportedMessageTypeException) {
				LoggerTools.logError(LOG,new StringBuilder().append("Received unsupported NMEA message: ")
						.append(string).toString());
			} catch (NMEAParseException parseException) {
				LoggerTools.logError(LOG,new StringBuilder().append("Received non-compliant NMEA message: ")
						.append(string).toString());
			}
		}
		LoggerTools.logInfo(LOG,"NMEAMessageInputStreamReader stopping.");
	}

	public final Boolean isStopRequested() {
		return this.stopRequested.get();
	}
	private final AtomicBoolean stopRequested = new AtomicBoolean(false);
	private final Supplier<String> stringSupplier;
	private final Consumer<? super NMEAMessage> nmeaMessageHandler;
}