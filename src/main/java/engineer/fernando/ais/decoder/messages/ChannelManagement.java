/*
 * ChannelManagement
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.messages.types.TxRxMode;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.*;

@SuppressWarnings("serial")
public class ChannelManagement extends AISMessage {

    private transient Integer channelA;
    private transient Integer channelB;
    private transient TxRxMode transmitReceiveMode;
    private transient Boolean power;
    private transient Float northEastLongitude;
    private transient Float northEastLatitude;
    private transient Float southWestLongitude;
    private transient Float southWestLatitude;
    private transient MMSI destinationMmsi1;
    private transient MMSI destinationMmsi2;
    private transient Boolean addressed;
    private transient Boolean bandA;
    private transient Boolean bandB;
    private transient Integer zoneSize;

    public ChannelManagement(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public ChannelManagement(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChannelManagement)) return false;
        if (!super.equals(o)) return false;
        ChannelManagement that = (ChannelManagement) o;
        return getChannelA().equals(that.getChannelA()) && getChannelB().equals(that.getChannelB())
                && getTransmitReceiveMode() == that.getTransmitReceiveMode() && getPower().equals(that.getPower())
                && getNorthEastLongitude().equals(that.getNorthEastLongitude()) && getNorthEastLatitude().equals(that.getNorthEastLatitude())
                && getSouthWestLongitude().equals(that.getSouthWestLongitude()) && getSouthWestLatitude().equals(that.getSouthWestLatitude())
                && getDestinationMmsi1().equals(that.getDestinationMmsi1()) && getDestinationMmsi2().equals(that.getDestinationMmsi2())
                && getAddressed().equals(that.getAddressed()) && getBandA().equals(that.getBandA()) && getBandB().equals(that.getBandB())
                && getZoneSize().equals(that.getZoneSize());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getChannelA(), getChannelB(), getTransmitReceiveMode(), getPower(), getNorthEastLongitude(), getNorthEastLatitude(), getSouthWestLongitude(), getSouthWestLatitude(), getDestinationMmsi1(), getDestinationMmsi2(), getAddressed(), getBandA(), getBandB(), getZoneSize());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.CHANNEL_MANAGEMENT;
    }

	public Integer getChannelA() {
        return getDecodedValue(() -> channelA, value -> channelA = value,  () -> Boolean.TRUE,
                 () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 52)));
	}

	public Integer getChannelB() {
        return getDecodedValue(() -> channelB, value -> channelB = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(52, 64)));
	}

	public TxRxMode getTransmitReceiveMode() {
        return getDecodedValue(() -> transmitReceiveMode, value -> transmitReceiveMode = value,
                () -> Boolean.TRUE, () -> TxRxMode.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(64, 68))));
	}

	public Boolean getPower() {
        return getDecodedValue(() -> power, value -> power = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(68, 69)));
	}

	public Float getNorthEastLongitude() {
        return getDecodedValue(() -> northEastLongitude, value -> northEastLongitude = value,
                () -> !getAddressed(), () -> FLOAT_DECODER.apply(getBits(69, 87)) / 10f);
	}

	public Float getNorthEastLatitude() {
        return getDecodedValue(() -> northEastLatitude, value -> northEastLatitude = value,
                () -> !getAddressed(), () -> FLOAT_DECODER.apply(getBits(87, 104)) / 10f);
	}

	public Float getSouthWestLongitude() {
        return getDecodedValue(() -> southWestLongitude, value -> southWestLongitude = value,
                () -> !getAddressed(), () -> FLOAT_DECODER.apply(getBits(104, 122)) / 10f);
	}

	public Float getSouthWestLatitude() {
        return getDecodedValue(() -> southWestLatitude, value -> southWestLatitude = value,
                () -> !getAddressed(), () -> FLOAT_DECODER.apply(getBits(122, 139)) / 10f);
	}

	public MMSI getDestinationMmsi1() {
        return getDecodedValue(() -> destinationMmsi1, value -> destinationMmsi1 = value,
                this::getAddressed, () -> MMSI.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(69, 99))));
	}

	public MMSI getDestinationMmsi2() {
        return getDecodedValue(() -> destinationMmsi2, value -> destinationMmsi2 = value,
                this::getAddressed, () -> MMSI.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(104, 134))));
	}

	public Boolean getAddressed() {
        return getDecodedValue(() -> addressed, value -> addressed = value,
                () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(139, 140)));
	}

	public Boolean getBandA() {
        return getDecodedValue(() -> bandA, value -> bandA = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(140, 141)));
	}

	public Boolean getBandB() {
        return getDecodedValue(() -> bandB, value -> bandB = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(141, 142)));
	}

	public Integer getZoneSize() {
        return getDecodedValue(() -> zoneSize, value -> zoneSize = value, ()
                -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(142, 145)));
	}

    @Override
    public String toString() {
        return "ChannelManagement{" +
                "messageType=" + getMessageType() +
                ", channelA=" + getChannelA() +
                ", channelB=" + getChannelB() +
                ", transmitReceiveMode=" + getTransmitReceiveMode() +
                ", power=" + getPower() +
                ", northEastLongitude=" + getNorthEastLongitude() +
                ", northEastLatitude=" + getNorthEastLatitude() +
                ", southWestLongitude=" + getSouthWestLongitude() +
                ", southWestLatitude=" + getSouthWestLatitude() +
                ", destinationMmsi1=" + getDestinationMmsi1() +
                ", destinationMmsi2=" + getDestinationMmsi2() +
                ", addressed=" + getAddressed() +
                ", bandA=" + getBandA() +
                ", bandB=" + getBandB() +
                ", zoneSize=" + getZoneSize() +
                "} " + super.toString();
    }

}