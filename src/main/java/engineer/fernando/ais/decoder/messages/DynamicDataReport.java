/*
 * DynamicDataReport
 */

package engineer.fernando.ais.decoder.messages;

public interface DynamicDataReport extends DataReport {
	Float getLatitude();
	Float getLongitude();
	Float getSpeedOverGround();
	Float getCourseOverGround();
}