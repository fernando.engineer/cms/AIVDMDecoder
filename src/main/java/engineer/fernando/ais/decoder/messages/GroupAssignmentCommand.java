/*
 * GroupAssignmentCommand
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.ReportingInterval;
import engineer.fernando.ais.decoder.messages.types.ShipType;
import engineer.fernando.ais.decoder.messages.types.StationType;
import engineer.fernando.ais.decoder.messages.types.TxRxMode;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.*;

public class GroupAssignmentCommand extends AISMessage {

    private transient String spare1;
    private transient Float northEastLatitude;
    private transient Float northEastLongitude;
    private transient Float southWestLatitude;
    private transient Float southWestLongitude;
    private transient StationType stationType;
    private transient ShipType shipType;
    private transient TxRxMode transmitReceiveMode;
    private transient ReportingInterval reportingInterval;
    private transient Integer quietTime;
    private transient String spare2;

    public GroupAssignmentCommand(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public GroupAssignmentCommand(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupAssignmentCommand)) return false;
        if (!super.equals(o)) return false;
        GroupAssignmentCommand that = (GroupAssignmentCommand) o;
        return getSpare1().equals(that.getSpare1()) && getNorthEastLatitude().equals(that.getNorthEastLatitude())
                && getNorthEastLongitude().equals(that.getNorthEastLongitude()) && getSouthWestLatitude().equals(that.getSouthWestLatitude())
                && getSouthWestLongitude().equals(that.getSouthWestLongitude()) && getStationType() == that.getStationType()
                && getShipType() == that.getShipType() && getTransmitReceiveMode() == that.getTransmitReceiveMode()
                && getReportingInterval() == that.getReportingInterval() && getQuietTime().equals(that.getQuietTime())
                && getSpare2().equals(that.getSpare2());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSpare1(), getNorthEastLatitude(), getNorthEastLongitude(), getSouthWestLatitude(),
                getSouthWestLongitude(), getStationType(), getShipType(), getTransmitReceiveMode(), getReportingInterval(), getQuietTime(),
                getSpare2());
    }


    public final AISMessageType getMessageType() {
        return AISMessageType.GROUP_ASSIGNMENT_COMMAND;
    }

    public String getSpare1() {
        return getDecodedValue(() -> spare1, value -> spare1 = value, () -> Boolean.TRUE, () -> STRING_DECODER.apply(getBits(38, 40)));
    }

    public Float getNorthEastLongitude() {
        return getDecodedValue(() -> northEastLongitude, value -> northEastLongitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(40, 58)) / 10f);
    }

	public Float getNorthEastLatitude() {
        return getDecodedValue(() -> northEastLatitude, value -> northEastLatitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(58, 75)) / 10f);
	}

	public Float getSouthWestLongitude() {
        return getDecodedValue(() -> southWestLongitude, value -> southWestLongitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(75, 93)) / 10f);
	}

    public Float getSouthWestLatitude() {
        return getDecodedValue(() -> southWestLatitude, value -> southWestLatitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(93, 110)) / 10f);
    }

	public StationType getStationType() {
        return getDecodedValue(() -> stationType, value -> stationType = value,
                () -> Boolean.TRUE, () -> StationType.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(110, 114))));
	}

	public ShipType getShipType() {
        return getDecodedValue(() -> shipType, value -> shipType = value,
                () -> Boolean.TRUE, () -> ShipType.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(114, 122))));
	}

    public String getSpare2() {
        return getDecodedValue(() -> spare2, value -> spare2 = value, () -> Boolean.TRUE, () -> STRING_DECODER.apply(getBits(122, 166)));
    }

	public TxRxMode getTransmitReceiveMode() {
        return getDecodedValue(() -> transmitReceiveMode, value -> transmitReceiveMode = value,
                () -> Boolean.TRUE, () -> TxRxMode.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(166, 168))));
	}

	public ReportingInterval getReportingInterval() {
        return getDecodedValue(() -> reportingInterval, value -> reportingInterval = value,
                () -> Boolean.TRUE, () -> ReportingInterval.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(168, 172))));
	}

	public Integer getQuietTime() {
        return getDecodedValue(() -> quietTime, value -> quietTime = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(172, 176)));
	}

    @Override
    public String toString() {
        return "GroupAssignmentCommand{" +
                "messageType=" + getMessageType() +
                ", spare1='" + getSpare1() + '\'' +
                ", northEastLatitude=" + getNorthEastLatitude() +
                ", northEastLongitude=" + getNorthEastLongitude() +
                ", southWestLatitude=" + getSouthWestLatitude() +
                ", southWestLongitude=" + getSouthWestLongitude() +
                ", stationType=" + getStationType() +
                ", shipType=" + getShipType() +
                ", transmitReceiveMode=" + getTransmitReceiveMode() +
                ", reportingInterval=" + getReportingInterval() +
                ", quietTime=" + getQuietTime() +
                ", spare2='" + getSpare2() + '\'' +
                "} " + super.toString();
    }

}