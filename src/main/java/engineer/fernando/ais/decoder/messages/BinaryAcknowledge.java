/*
 * BinaryAcknowledge
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;

import java.util.Objects;


/**
 * Acknowledgement for type 6 messages.
 */

public class BinaryAcknowledge extends AISMessage {

    private transient Integer spare;
    private transient MMSI mmsi1;
    private transient Integer sequence1;
    private transient MMSI mmsi2;
    private transient Integer sequence2;
    private transient MMSI mmsi3;
    private transient Integer sequence3;
    private transient MMSI mmsi4;
    private transient Integer sequence4;
    private transient Integer numOfAcks;

    public BinaryAcknowledge(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public BinaryAcknowledge(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BinaryAcknowledge)) return false;
        if (!super.equals(o)) return false;
        BinaryAcknowledge that = (BinaryAcknowledge) o;
        return getSpare().equals(that.getSpare()) && getMmsi1().equals(that.getMmsi1())
                && getSequence1().equals(that.getSequence1()) && getMmsi2().equals(that.getMmsi2())
                && getSequence2().equals(that.getSequence2()) && getMmsi3().equals(that.getMmsi3())
                && getSequence3().equals(that.getSequence3()) && getMmsi4().equals(that.getMmsi4())
                && getSequence4().equals(that.getSequence4()) && getNumOfAcks().equals(that.getNumOfAcks());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSpare(), getMmsi1(), getSequence1(), getMmsi2(), getSequence2(),
                getMmsi3(), getSequence3(), getMmsi4(), getSequence4(), getNumOfAcks());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.BINARY_ACKNOWLEDGE;
    }

	public Integer getSpare() {
        return getDecodedValue(() -> spare, value -> spare = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 40)));
	}

	public MMSI getMmsi1() {
        return getDecodedValue(() -> mmsi1, value -> mmsi1 = value, () -> Boolean.TRUE,
                () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
	}

	public Integer getSequence1() {
        return getDecodedValue(() -> sequence1, value -> sequence1 = value,
                () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(70, 72)));
	}

	public MMSI getMmsi2() {
        return getDecodedValue(() -> mmsi2, value -> mmsi2 = value, () -> getNumberOfBits() > 72,
                () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(72, 102))));
	}

	public Integer getSequence2() {
        return getDecodedValue(() -> sequence2, value -> sequence2 = value, () -> getNumberOfBits() > 72,
                () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(102, 104)));
	}

	public MMSI getMmsi3() {
        return getDecodedValue(() -> mmsi3, value -> mmsi3 = value, () -> getNumberOfBits() > 104,
                () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(104, 134))));
	}

	public Integer getSequence3() {
        return getDecodedValue(() -> sequence3, value -> sequence3 = value, () -> getNumberOfBits() > 104,
                () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(134, 136)));
	}

	public MMSI getMmsi4() {
        return getDecodedValue(() -> mmsi4, value -> mmsi4 = value, () -> getNumberOfBits() > 136,
                () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(136, 166))));
	}

	public Integer getSequence4() {
        return getDecodedValue(() -> sequence4, value -> sequence4 = value,
                () -> getNumberOfBits() > 136, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(166, 168)));
	}

	public Integer getNumOfAcks() {
        if (numOfAcks == null) {
            final int numberOfBits = getNumberOfBits();
            numOfAcks = 1;
            if (numberOfBits > 72) {
                numOfAcks++;
            }
            if (numberOfBits > 104) {
                numOfAcks++;
            }
            if (numberOfBits > 136) {
                numOfAcks++;
            }
        }
        return numOfAcks;
	}

    @Override
    public String toString() {
        return "BinaryAcknowledge{" +
                "messageType=" + getMessageType() +
                ", spare=" + getSpare() +
                ", mmsi1=" + getMmsi1() +
                ", sequence1=" + getSequence1() +
                ", mmsi2=" + getMmsi2() +
                ", sequence2=" + getSequence2() +
                ", mmsi3=" + getMmsi3() +
                ", sequence3=" + getSequence3() +
                ", mmsi4=" + getMmsi4() +
                ", sequence4=" + getSequence4() +
                ", numOfAcks=" + getNumOfAcks() +
                "} " + super.toString();
    }

}