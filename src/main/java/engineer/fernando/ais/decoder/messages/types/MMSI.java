/*
 * MMSI
 */

package engineer.fernando.ais.decoder.messages.types;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MMSI implements Serializable {

	public MMSI(int mmsi) {
		this.mmsiValue = mmsi;
	}
	
	public static MMSI valueOf(int mmsi) {
		return new MMSI(mmsi);
	}
	
	@Override
	public String toString() {
		return "MMSI [mmsi=" + mmsiValue + "]";
	}

	public Integer getMMSI() {
	    return Integer.valueOf(mmsiValue);
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        var mmsi1 = (MMSI) o;

        return mmsiValue == mmsi1.mmsiValue;
    }

    @Override
    public int hashCode() {
        return mmsiValue;
    }

    private final int mmsiValue;
}