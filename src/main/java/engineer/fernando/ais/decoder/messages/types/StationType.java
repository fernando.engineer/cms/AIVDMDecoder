/*
 * StationType
 */

package engineer.fernando.ais.decoder.messages.types;

public enum StationType {
	ALL_TYPES_OF_MOBILE_STATIONS(0),
	CLASS_A_MOBILE_STATION(1),
	ALL_TYPES_OF_CLASS_B_MOBILE_STATIONS(2),
	SAR_AIRBORNE_MOBILE_STATION(3),
	CLASS_BSO_MOBILE_STATION(4),
	CLASS_BCS_SHIPBORNE_MOBILE_STATION(5),
	INLAND_WATERWAYS(6),
	REGIONAL_USE_1(7),
	REGIONAL_USE_2(8),
	REGIONAL_USE_3(9),
	BASE_STATION_COVERAGE_AREA(10),
	FUTURE_USE_1(11),
	FUTURE_USE_2(12),
	FUTURE_USE_3(13),
	FUTURE_USE_4(14),
	FUTURE_USE_5(15);
	
	StationType(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	private final Integer code;

	public static StationType fromInteger(Integer integer) {
		if (integer != null) {
			for (StationType b : StationType.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}
}