/*
 * ExtendedDynamicDataReport
 */

package engineer.fernando.ais.decoder.messages;

public interface ExtendedDynamicDataReport extends DynamicDataReport {
	Integer getTrueHeading();
	Integer getSecond();
}