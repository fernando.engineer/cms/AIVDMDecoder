/*
 * AssignedModeCommand
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;
import java.util.Objects;


public class AssignedModeCommand extends AISMessage {

    private transient MMSI destinationMmsiA;
    private transient Integer offsetA;
    private transient Integer incrementA;
    private transient MMSI destinationMmsiB;
    private transient Integer offsetB;
    private transient Integer incrementB;

    public AssignedModeCommand(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public AssignedModeCommand(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssignedModeCommand)) return false;
        if (!super.equals(o)) return false;
        AssignedModeCommand that = (AssignedModeCommand) o;
        return getDestinationMmsiA().equals(that.getDestinationMmsiA()) && getOffsetA().equals(that.getOffsetA())
                && getIncrementA().equals(that.getIncrementA()) && getDestinationMmsiB().equals(that.getDestinationMmsiB())
                && getOffsetB().equals(that.getOffsetB()) && getIncrementB().equals(that.getIncrementB());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDestinationMmsiA(), getOffsetA(), getIncrementA(),
                getDestinationMmsiB(), getOffsetB(), getIncrementB());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.ASSIGNED_MODE_COMMAND;
    }

    public MMSI getDestinationMmsiA() {
        return getDecodedValue(() -> destinationMmsiA, value -> destinationMmsiA = value, () -> Boolean.TRUE,
                () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
    }

    public Integer getOffsetA() {
        return getDecodedValue(() -> offsetA, value -> offsetA = value, () -> Boolean.TRUE,
                () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(70, 82)));
    }

    public Integer getIncrementA() {
        return getDecodedValue(() -> incrementA, value -> incrementA = value, () -> Boolean.TRUE,
                () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(82, 92)));
    }

    public MMSI getDestinationMmsiB() {
        return getDecodedValue(() -> destinationMmsiB, value -> destinationMmsiB = value,
                () -> getNumberOfBits() >= 144, () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(92, 122))));
    }

    public Integer getOffsetB() {
        return getDecodedValue(() -> offsetB, value -> offsetB = value,
                () -> getNumberOfBits() >= 144, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(122, 134)));
    }

    public Integer getIncrementB() {
        return getDecodedValue(() -> incrementB, value -> incrementB = value,
                () -> getNumberOfBits() >= 144, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(134, 144)));
    }

    @Override
    public String toString() {
        return "AssignedModeCommand{" +
                "messageType=" + getMessageType() +
                ", destinationMmsiA=" + getDestinationMmsiA() +
                ", offsetA=" + getOffsetA() +
                ", incrementA=" + getIncrementA() +
                ", destinationMmsiB=" + getDestinationMmsiB() +
                ", offsetB=" + getOffsetB() +
                ", incrementB=" + getIncrementB() +
                "} " + super.toString();
    }
}