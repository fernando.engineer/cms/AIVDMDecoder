/*
 * UTCAndDateInquiry
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import static engineer.fernando.ais.decoder.util.Decoders.UNSIGNED_INTEGER_DECODER;

public class UTCAndDateInquiry extends AISMessage {

    private transient MMSI destinationMmsi;

    public UTCAndDateInquiry(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public UTCAndDateInquiry(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode(){
        return super.hashCode();
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.UTC_AND_DATE_INQUIRY;
    }

	public MMSI getDestinationMmsi() {
        return getDecodedValue(() -> destinationMmsi, value -> destinationMmsi = value, () -> Boolean.TRUE,
                () -> MMSI.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
	}

    @Override
    public String toString() {
        return "UTCAndDateInquiry{" +
                "messageType=" + getMessageType() +
                ", destinationMmsi=" + getDestinationMmsi() +
                "} " + super.toString();
    }

}