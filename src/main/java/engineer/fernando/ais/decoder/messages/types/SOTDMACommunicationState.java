/*
 * SOTDMACommunicationState
 */

package engineer.fernando.ais.decoder.messages.types;

import java.io.Serializable;

import engineer.fernando.ais.decoder.util.StringTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static engineer.fernando.ais.decoder.util.Decoders.UNSIGNED_INTEGER_DECODER;
import static java.util.Objects.requireNonNull;

public class SOTDMACommunicationState extends CommunicationState implements Serializable {

	private static final Logger LOG = LogManager.getLogger(SOTDMACommunicationState.class);
	private static final long serialVersionUID = 1L;
	private Integer slotTimeout;
	private Integer numberOfReceivedStations;
	private Integer slotNumber;
	private Integer utcHour;
	private Integer utcMinute;
	private Integer slotOffset;

	private SOTDMACommunicationState(SyncState syncState, Integer slotTimeout, Integer numberOfReceivedStations, Integer slotNumber, Integer utcHour, Integer utcMinute, Integer slotOffset) {
		super(syncState);
		this.slotTimeout = slotTimeout;
		this.numberOfReceivedStations = numberOfReceivedStations;
		this.slotNumber = slotNumber;
		this.utcHour = utcHour;
		this.utcMinute = utcMinute;
		this.slotOffset = slotOffset;
	}

	public static SOTDMACommunicationState fromBitString(String bitString) {
		requireNonNull(bitString);
		bitString = bitString.trim();

		if (bitString.length() != 19 || !bitString.matches("[01]*"))
			return null;

		var syncState = SyncState.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(0, 2)));
		final var slotTimeout = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(2, 5));
		Integer numberOfReceivedStations=null;
		Integer slotNumber=null;
		Integer utcHour=null;
		Integer utcMinute=null;
		Integer slotOffset=null;

		if (slotTimeout == 3 || slotTimeout == 5 || slotTimeout == 7) {
			numberOfReceivedStations = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(5, 19));
			outOfRangeWarningCheck("numberOfReceivedStations", 16383,numberOfReceivedStations );
		} else if (slotTimeout == 2 || slotTimeout == 4 || slotTimeout == 6) {
			slotNumber = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(5, 19));
			outOfRangeWarningCheck("slotNumber", 2249,slotNumber );
		}  else if (slotTimeout == 1) {
			utcHour = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(5, 10));
			outOfRangeWarningCheck("utcHour", 23,utcHour );
			utcMinute = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(10, 17));
			outOfRangeWarningCheck("utcMinute", 59,utcMinute );
		}  else if (slotTimeout == 0) {
			slotOffset = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(5, 19));
		}
		return new SOTDMACommunicationState(syncState, slotTimeout, numberOfReceivedStations, slotNumber, utcHour, utcMinute, slotOffset);
	}

	private static void outOfRangeWarningCheck(String variable, int max, int current) {
		if (current > max){
			LOG.warn(StringTools.outOfRange(variable,current));
		}
	}

	public Integer getSlotTimeout() {
		return slotTimeout;
	}

	public Integer getNumberOfReceivedStations() {
		return numberOfReceivedStations;
	}

	public Integer getSlotNumber() {
		return slotNumber;
	}

	public Integer getUtcHour() {
		return utcHour;
	}

	public Integer getUtcMinute() {
		return utcMinute;
	}

	public Integer getSlotOffset() {
		return slotOffset;
	}

	@Override
	public String toString() {
		return "SOTDMACommunicationState{" +
				super.toString() +
				", slotTimeout=" + slotTimeout +
				", numberOfReceivedStations=" + numberOfReceivedStations +
				", slotNumber=" + slotNumber +
				", utcHour=" + utcHour +
				", utcMinute=" + utcMinute +
				", slotOffset=" + slotOffset +
				"}";
	}
}