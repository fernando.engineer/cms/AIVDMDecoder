/*
 * BinaryMessageMultipleSlot
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;

@SuppressWarnings("serial")
public class BinaryMessageMultipleSlot extends AISMessage {

    private transient Boolean addressed;
    private transient Boolean structured;
    private transient MMSI destinationMmsi;
    private transient Integer applicationId;
    private transient String data;

    public BinaryMessageMultipleSlot(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public BinaryMessageMultipleSlot(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.BINARY_MESSAGE_MULTIPLE_SLOT;
    }

    @SuppressWarnings("unused")
    public Boolean getAddressed() {
        return getDecodedValue(() -> addressed, value -> addressed = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(38, 39)));
    }

    @SuppressWarnings("unused")
    public Boolean getStructured() {
        return getDecodedValue(() -> structured, value -> structured = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(39, 40)));
    }

    @SuppressWarnings("unused")
    public MMSI getDestinationMmsi() {
        return getDecodedValue(() -> destinationMmsi, value -> destinationMmsi = value, () -> Boolean.TRUE,
                () -> MMSI.valueOf(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
    }

    @SuppressWarnings("unused")
    public Integer getApplicationId() {
        return getDecodedValue(() -> applicationId, value -> applicationId = value,
                () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(70, 86)));
    }

    @SuppressWarnings("unused")
    public String getData() {
        return getDecodedValue(() -> data, value -> data = value, () -> Boolean.TRUE, () -> Decoders.BIT_DECODER.apply(getBits(86, 86 + 1004 + 1)));
    }

    @SuppressWarnings("unused")
    public String getRadioStatus() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode(){
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "BinaryMessageMultipleSlot{" +
                "messageType=" + getMessageType() +
                ", addressed=" + getAddressed() +
                ", structured=" + getStructured() +
                ", destinationMmsi=" + getDestinationMmsi() +
                ", applicationId=" + getApplicationId() +
                ", data='" + getData() + '\'' +
                ", radioStatus='" + getRadioStatus() + '\'' +
                "} " + super.toString();
    }

}