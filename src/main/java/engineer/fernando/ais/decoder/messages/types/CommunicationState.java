/*
 * CommunicationState
 */

package engineer.fernando.ais.decoder.messages.types;

import java.io.Serializable;

public abstract class CommunicationState implements Serializable {

	protected CommunicationState(SyncState syncState) {
		this.syncState = syncState;
	}

	@SuppressWarnings("unused")
	public SyncState getSyncState() {
		return syncState;
	}

	private final SyncState syncState;

	@Override
	public String toString() {
		return "CommunicationState{" +
				"syncState=" + syncState +
				'}';
	}
}