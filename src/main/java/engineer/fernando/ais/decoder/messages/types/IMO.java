/*
 * IMO
 */

package engineer.fernando.ais.decoder.messages.types;

import java.io.Serializable;

public class IMO implements Serializable {

	public IMO(int imo) {
		this.imoValue = imo;
	}
	
	public static IMO valueOf(int imo) {
		return new IMO(imo);
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        var imo1 = (IMO) o;

        return imoValue == imo1.imoValue;
    }

    @Override
    public int hashCode() {
        return imoValue;
    }

    @Override
	public String toString() {
		return "IMO [imo=" + imoValue + "]";
	}

    public Integer getIMO() {
	    return imoValue;
	}

	private final int imoValue;
}