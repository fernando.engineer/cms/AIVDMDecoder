/*
 * NavigationStatus
 */

package engineer.fernando.ais.decoder.messages.types;

public enum NavigationStatus {
	UNDERWAY_USING_ENGINE(0),
	AT_ANCHOR(1),
	NOT_UNDER_COMMAND(2),
	RESTRICTED_MANOEUVERABILITY(3),
	CONSTRAINED_BY_HER_DRAUGHT(4),
	MOORED(5),
	AGROUND(6),
	ENGAGED_IN_FISHING(7),
	UNDERWAY_SAILING(8),
	RESERVED_FOR_FUTURE_USE_9(9),
	RESERVED_FOR_FUTURE_USE_10(10),
	POWER_DRIVEN_VESSEL_TOWING_ASTERN(11),
	POWER_DRIVEN_VESSEL_PUSHING_AHEAD_OR_TOWING_ALONGSIDE(12),
	RESERVED_FOR_FUTURE_USE_13(13),
	SART_MOB_OR_EPIRB(14),
	UNDEFINED(15);

	NavigationStatus(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	private final Integer code;

	public static NavigationStatus fromInteger(Integer integer) {
		if (integer != null) {
			for (NavigationStatus b : NavigationStatus.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}
}