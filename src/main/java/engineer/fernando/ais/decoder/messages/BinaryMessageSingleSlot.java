/*
 * BinaryMessageSingleSlot
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.*;

@SuppressWarnings("serial")
public class BinaryMessageSingleSlot extends AISMessage {

    private transient Boolean destinationIndicator;
    private transient Boolean binaryDataFlag;
    private transient MMSI destinationMMSI;
    private transient String binaryData;

    public BinaryMessageSingleSlot(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public BinaryMessageSingleSlot(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BinaryMessageSingleSlot)) return false;
        if (!super.equals(o)) return false;
        BinaryMessageSingleSlot that = (BinaryMessageSingleSlot) o;
        return getDestinationIndicator().equals(that.getDestinationIndicator()) && getBinaryDataFlag().equals(that.getBinaryDataFlag())
                && getDestinationMMSI().equals(that.getDestinationMMSI()) && getBinaryData().equals(that.getBinaryData());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getDestinationIndicator(), getBinaryDataFlag(), getDestinationMMSI(), getBinaryData());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.BINARY_MESSAGE_SINGLE_SLOT;
    }

	public Boolean getDestinationIndicator() {
        return getDecodedValue(() -> destinationIndicator, value -> destinationIndicator = value,
                () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(38, 39)));
	}

	public Boolean getBinaryDataFlag() {
        return getDecodedValue(() -> binaryDataFlag, value -> binaryDataFlag = value,
                () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(39, 40)));
	}

	public MMSI getDestinationMMSI() {
        return getDecodedValue(() -> destinationMMSI, value -> destinationMMSI = value,
                () -> Boolean.TRUE, () -> MMSI.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
	}

	public String getBinaryData() {
        return getDecodedValue(() -> binaryData, value -> binaryData = value, () -> Boolean.TRUE, () -> BIT_DECODER.apply(getBits(40, 168)));
	}

    @Override
    public String toString() {
        return "BinaryMessageSingleSlot{" +
                "messageType=" + getMessageType() +
                ", destinationIndicator=" + getDestinationIndicator() +
                ", destinationMMSI=" + getDestinationMMSI() +
                ", binaryDataFlag=" + getBinaryDataFlag() +
                ", binaryData='" + getBinaryData() + '\'' +
                "} " + super.toString();
    }

}