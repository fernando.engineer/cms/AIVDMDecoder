/*
 * Interrogation
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.MMSI;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.UNSIGNED_INTEGER_DECODER;

public class Interrogation extends AISMessage {

    private transient MMSI interrogatedMmsi1;
    private transient Integer type11;
    private transient Integer offset11;
    private transient Integer type12;
    private transient Integer offset12;
    private transient MMSI interrogatedMmsi2;
    private transient Integer type21;
    private transient Integer offset21;

    public Interrogation(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public Interrogation(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Interrogation)) return false;
        if (!super.equals(o)) return false;
        Interrogation that = (Interrogation) o;
        return getInterrogatedMmsi1().equals(that.getInterrogatedMmsi1()) && getType11().equals(that.getType11())
                && getOffset11().equals(that.getOffset11()) && getType12().equals(that.getType12()) && getOffset12().equals(that.getOffset12())
                && getInterrogatedMmsi2().equals(that.getInterrogatedMmsi2()) && getType21().equals(that.getType21())
                && getOffset21().equals(that.getOffset21());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getInterrogatedMmsi1(), getType11(), getOffset11(), getType12(), getOffset12(), getInterrogatedMmsi2(),
                getType21(), getOffset21());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.INTERROGATION;
    }

	public final MMSI getInterrogatedMmsi1() {
        return getDecodedValue(() -> interrogatedMmsi1, value -> interrogatedMmsi1 = value,
                () -> Boolean.TRUE, () -> MMSI.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(40, 70))));
	}

	public final Integer getType11() {
        return getDecodedValue(() -> type11, value -> type11 = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(70, 76)));
	}

	public final Integer getOffset11() {
        return getDecodedValue(() -> offset11, value -> offset11 = value,
                () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(76, 88)));
	}

	public final Integer getType12() {
        return getDecodedValue(() -> type12, value -> type12 = value,
                () -> getNumberOfBits() > 88, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(90, 96)));
	}

	public final Integer getOffset12() {
        return getDecodedValue(() -> offset12, value -> offset12 = value,
                () -> getNumberOfBits() > 88, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(96, 108)));
	}

	public final MMSI getInterrogatedMmsi2() {
        return getDecodedValue(() -> interrogatedMmsi2, value -> interrogatedMmsi2 = value,
                () -> getNumberOfBits() >= 110, () -> MMSI.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(110, 140))));
	}

	public final Integer getType21() {
        return getDecodedValue(() -> type21, value -> type21 = value,
                () -> getNumberOfBits() >= 110, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(140, 146)));
	}

	public final Integer getOffset21() {
        return getDecodedValue(() -> offset21, value -> offset21 = value,
                () -> getNumberOfBits() >= 110, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(146, 158)));
	}

    @Override
    public String toString() {
        return "Interrogation{" +
                "messageType=" + getMessageType() +
                ", interrogatedMmsi1=" + getInterrogatedMmsi1() +
                ", type1_1=" + getType11() +
                ", offset1_1=" + getOffset11() +
                ", type1_2=" + getType12() +
                ", offset1_2=" + getOffset12() +
                ", interrogatedMmsi2=" + getInterrogatedMmsi2() +
                ", type2_1=" + getType21() +
                ", offset2_1=" + getOffset21() +
                "} " + super.toString();
    }

}