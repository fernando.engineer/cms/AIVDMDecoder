/*
 * PositionReport
 */

package engineer.fernando.ais.decoder.messages;
import engineer.fernando.ais.decoder.messages.types.CommunicationState;
import engineer.fernando.ais.decoder.messages.types.ITDMACommunicationState;
import engineer.fernando.ais.decoder.messages.types.ManeuverIndicator;
import engineer.fernando.ais.decoder.messages.types.NavigationStatus;
import engineer.fernando.ais.decoder.messages.types.SOTDMACommunicationState;
import engineer.fernando.ais.decoder.messages.types.TransponderClass;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import java.lang.ref.WeakReference;
import java.util.Objects;
import static engineer.fernando.ais.decoder.util.Decoders.*;

@SuppressWarnings("serial")
public abstract class PositionReport extends AISMessage implements ExtendedDynamicDataReport {

    private transient NavigationStatus navigationStatus;
    private transient Integer rateOfTurn;
    private transient Float speedOverGround;
    private transient Boolean positionAccuracy;
    private transient Float latitude;
    private transient Float longitude;
    private transient Float courseOverGround;
    private transient Integer trueHeading;
    private transient Integer second;
    private transient ManeuverIndicator specialManeuverIndicator;
    private transient Boolean raimFlag;
    private transient WeakReference<CommunicationState> communicationState;

    protected PositionReport(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    protected PositionReport(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PositionReport)) return false;
        if (!super.equals(o)) return false;
        PositionReport that = (PositionReport) o;
        return getNavigationStatus() == that.getNavigationStatus() && Objects.equals(getRateOfTurn(), that.getRateOfTurn()) && Objects.equals(getSpeedOverGround(), that.getSpeedOverGround()) && Objects.equals(getPositionAccuracy(), that.getPositionAccuracy()) && Objects.equals(getLatitude(), that.getLatitude()) && Objects.equals(getLongitude(), that.getLongitude()) && Objects.equals(getCourseOverGround(), that.getCourseOverGround()) && Objects.equals(getTrueHeading(), that.getTrueHeading()) && Objects.equals(getSecond(), that.getSecond()) && getSpecialManeuverIndicator() == that.getSpecialManeuverIndicator() && Objects.equals(getRaimFlag(), that.getRaimFlag()) && Objects.equals(getCommunicationState(), that.getCommunicationState());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getNavigationStatus(), getRateOfTurn(), getSpeedOverGround(), getPositionAccuracy(), getLatitude(), getLongitude(), getCourseOverGround(), getTrueHeading(), getSecond(), getSpecialManeuverIndicator(), getRaimFlag(), getCommunicationState());
    }

    @Override
    public TransponderClass getTransponderClass() {
        return TransponderClass.A;
    }

    @SuppressWarnings("unused")
    public NavigationStatus getNavigationStatus() {
        return getDecodedValue(() -> navigationStatus, value -> navigationStatus = value, () -> Boolean.TRUE,
                () -> NavigationStatus.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 42))));
    }

    @SuppressWarnings("unused")
    public Integer getRateOfTurn() {
        final var factor = 4.733;
        var prevalue = getDecodedValue(() -> rateOfTurn, value -> rateOfTurn = value, () -> Boolean.TRUE,
                () -> INTEGER_DECODER.applyAsInt(getBits(42, 50)));
        var modvalue = Math.toIntExact(Math.round(Math.pow(prevalue/factor,2)));
        return prevalue > 0 ? modvalue : modvalue * -1;
    }

    @SuppressWarnings("unused")
    public Float getSpeedOverGround() {
        return getDecodedValue(() -> speedOverGround, value -> speedOverGround = value, () -> Boolean.TRUE,
                () -> UNSIGNED_FLOAT_DECODER.apply(getBits(50, 60)) / 10f);
    }


    @SuppressWarnings("unused")
    public Boolean getPositionAccuracy() {
        return getDecodedValue(() -> positionAccuracy, value -> positionAccuracy = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(60, 61)));
    }

    @SuppressWarnings("unused")
    public Float getLatitude() {
        return getDecodedValue(() -> latitude, value -> latitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(89, 116)) / 600000f);
    }


    @SuppressWarnings("unused")
    public Float getLongitude() {
        return getDecodedValue(() -> longitude, value -> longitude = value, () -> Boolean.TRUE, () -> FLOAT_DECODER.apply(getBits(61, 89)) / 600000f);
    }

    @SuppressWarnings("unused")
    public Float getCourseOverGround() {
        return getDecodedValue(() -> courseOverGround, value -> courseOverGround = value,
                () -> Boolean.TRUE, () -> UNSIGNED_FLOAT_DECODER.apply(getBits(116, 128)) / 10f);
    }

    @SuppressWarnings("unused")
    public Integer getTrueHeading() {
        return getDecodedValue(() -> trueHeading, value -> trueHeading = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(128, 137)));
    }

    @SuppressWarnings("unused")
    public Integer getSecond() {
        return getDecodedValue(() -> second, value -> second = value, () -> Boolean.TRUE, () -> UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(137, 143)));
    }

    @SuppressWarnings("unused")
    public ManeuverIndicator getSpecialManeuverIndicator() {
        return getDecodedValue(() -> specialManeuverIndicator, value -> specialManeuverIndicator = value, () -> Boolean.TRUE,
                () -> ManeuverIndicator.fromInteger(UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(143, 145))));
    }

    @SuppressWarnings("unused")
    public Boolean getRaimFlag() {
        return getDecodedValue(() -> raimFlag, value -> raimFlag = value, () -> Boolean.TRUE, () -> BOOLEAN_DECODER.test(getBits(148, 149)));
    }

    @SuppressWarnings("unused")
    public CommunicationState getCommunicationState() {
        if (this instanceof PositionReportClassAScheduled || this instanceof PositionReportClassAAssignedSchedule)
            return getDecodedValueByWeakReference(() -> communicationState, value -> communicationState = value, () -> Boolean.TRUE, () -> SOTDMACommunicationState.fromBitString(getBits(149, 168)));
        else if (this instanceof PositionReportClassAResponseToInterrogation)
            return getDecodedValueByWeakReference(() -> communicationState, value -> communicationState = value, () -> Boolean.TRUE, () -> ITDMACommunicationState.fromBitString(getBits(149, 168)));
        else
            return null;
    }

    @Override
    public String toString() {
        return "PositionReport{" +
                "navigationStatus=" + getNavigationStatus() +
                ", rateOfTurn=" + getRateOfTurn() +
                ", speedOverGround=" + getSpeedOverGround() +
                ", positionAccuracy=" + getPositionAccuracy() +
                ", latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                ", courseOverGround=" + getCourseOverGround() +
                ", trueHeading=" + getTrueHeading() +
                ", second=" + getSecond() +
                ", specialManeuverIndicator=" + getSpecialManeuverIndicator() +
                ", raimFlag=" + getRaimFlag() +
                ", communicationState=" + getCommunicationState() +
                "} " + super.toString();
    }

}