/*
 * TxRxMode
 */

package engineer.fernando.ais.decoder.messages.types;

public enum TxRxMode {
	TX_AB_RX_AB(0),
	TX_A_RX_AB(1),
	TX_B_RX_AB(2),
	FUTURE_USE(3);

	TxRxMode(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}

	public String getValue() {
	    return toString();
	}

	private final Integer code;

	public static TxRxMode fromInteger(Integer integer) {
		if (integer != null) {
			for (TxRxMode b : TxRxMode.values()) {
				if (integer.equals(b.code)) {
					return b;
				}
			}
		}
		return null;
	}
}