/*
 * BaseStationReport
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.types.PositionFixingDevice;
import engineer.fernando.ais.decoder.messages.types.SOTDMACommunicationState;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import engineer.fernando.ais.decoder.util.Decoders;

import java.lang.ref.WeakReference;
import java.util.Objects;

/**
 * Used by fixed-location base stations to report position and time reference.
 */

public class BaseStationReport extends AISMessage {

    private transient Integer year;
    private transient Integer month;
    private transient Integer day;
    private transient Integer hour;
    private transient Integer minute;
    private transient Integer second;
    private transient Boolean positionAccurate;
    private transient Float latitude;
    private transient Float longitude;
    private transient PositionFixingDevice positionFixingDevice;
    private transient Boolean raimFlag;
    private transient WeakReference<SOTDMACommunicationState> communicationState;

    public BaseStationReport(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public BaseStationReport(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseStationReport)) return false;
        if (!super.equals(o)) return false;
        BaseStationReport that = (BaseStationReport) o;
        return getYear().equals(that.getYear()) && getMonth().equals(that.getMonth()) && getDay().equals(that.getDay())
                && getHour().equals(that.getHour()) && getMinute().equals(that.getMinute())
                && getSecond().equals(that.getSecond()) && getPositionAccurate().equals(that.getPositionAccurate())
                && getLatitude().equals(that.getLatitude()) && getLongitude().equals(that.getLongitude())
                && getPositionFixingDevice() == that.getPositionFixingDevice() && getRaimFlag().equals(that.getRaimFlag())
                && getCommunicationState().equals(that.getCommunicationState());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getYear(), getMonth(), getDay(), getHour(), getMinute(), getSecond(),
                getPositionAccurate(), getLatitude(), getLongitude(), getPositionFixingDevice(), getRaimFlag(),
                getCommunicationState());
    }

    public final AISMessageType getMessageType() {
        return AISMessageType.BASE_STATION_REPORT;
    }

	public Integer getYear() {
        return getDecodedValue(() -> year, value -> year = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(38, 52)));
	}

	public Integer getMonth() {
        return getDecodedValue(() -> month, value -> month = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(52, 56)));
	}

	public Integer getDay() {
        return getDecodedValue(() -> day, value -> day = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(56, 61)));
	}

	public Integer getHour() {
        return getDecodedValue(() -> hour, value -> hour = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(61, 66)));
	}

	public Integer getMinute() {
        return getDecodedValue(() -> minute, value -> minute = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(66, 72)));
	}

	public Integer getSecond() {
        return getDecodedValue(() -> second, value -> second = value, () -> Boolean.TRUE, () -> Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(72, 78)));
	}

	public Boolean getPositionAccurate() {
        return getDecodedValue(() -> positionAccurate, value -> positionAccurate = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(78, 79)));
	}

	public Float getLatitude() {
        return getDecodedValue(() -> latitude, value -> latitude = value, () -> Boolean.TRUE, () -> Decoders.FLOAT_DECODER.apply(getBits(107, 134)) / 600000f);
	}

	public Float getLongitude() {
        return getDecodedValue(() -> longitude, value -> longitude = value, () -> Boolean.TRUE, () -> Decoders.FLOAT_DECODER.apply(getBits(79, 107)) / 600000f);
	}

	public PositionFixingDevice getPositionFixingDevice() {
        return getDecodedValue(() -> positionFixingDevice, value -> positionFixingDevice = value, () -> Boolean.TRUE,
                () -> PositionFixingDevice.fromInteger(Decoders.UNSIGNED_INTEGER_DECODER.applyAsInt(getBits(134, 138))));
	}

	public Boolean getRaimFlag() {
        return getDecodedValue(() -> raimFlag, value -> raimFlag = value, () -> Boolean.TRUE, () -> Decoders.BOOLEAN_DECODER.test(getBits(148, 149)));
	}

    public SOTDMACommunicationState getCommunicationState() {
        return getDecodedValueByWeakReference(() -> communicationState, value -> communicationState = value, () -> Boolean.TRUE, () -> SOTDMACommunicationState.fromBitString(getBits(149, 168)));
    }

    @Override
    public String toString() {
        return "BaseStationReport{" +
                "messageType=" + getMessageType() +
                ", year=" + getYear() +
                ", month=" + getMonth() +
                ", day=" + getDay() +
                ", hour=" + getHour() +
                ", minute=" + getMinute() +
                ", second=" + getSecond() +
                ", positionAccurate=" + getPositionAccurate() +
                ", latitude=" + getLatitude() +
                ", longitude=" + getLongitude() +
                ", positionFixingDevice=" + getPositionFixingDevice() +
                ", raimFlag=" + getRaimFlag() +
                ", communicationState=" + getCommunicationState() +
                "} " + super.toString();
    }

}