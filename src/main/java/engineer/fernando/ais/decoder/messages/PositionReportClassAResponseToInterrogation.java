/*
 * PositionReportClassAResponseToInterrogation
 */

package engineer.fernando.ais.decoder.messages;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;

@SuppressWarnings("serial")
public class PositionReportClassAResponseToInterrogation extends PositionReport {
    public PositionReportClassAResponseToInterrogation(NMEAMessage[] nmeaMessages) {
        super(nmeaMessages);
    }

    public PositionReportClassAResponseToInterrogation(NMEAMessage[] nmeaMessages, String bitString) {
        super(nmeaMessages, bitString);
    }

    @Override
    public String toString() {
        return "PositionReportClassAResponseToInterrogation{" +
                "messageType=" + getMessageType() +
                "} " + super.toString();
    }

    @Override
    public AISMessageType getMessageType() {
        return AISMessageType.POSITION_REPORT_CLASS_A_RESPONSE_TO_INTERROGATION;
    }
}