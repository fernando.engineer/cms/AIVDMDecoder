package engineer.fernando.ais.decoder.util;

public class StringTools {

    public static final String SIMPLE_FORMATER = " %s: %s";
    public static final String DATE_SIMPLE_FORMATER = "%02d-%02d %02d:%02d";
    public static final String WARNING_TOO_SHORT = "Message is too short: ";
    public static final String MESSAGE_TYPE = "Message type ";
    public static final String ILLEGAL_LENGTH = "Illegal message length: ";
    private static final String TOO_SHORT_TO_DETERMINE_TYPE = "Message is too short to determine message type: %d bits.";
    private static final String UNSUPPORTED_MESSAGE = "Unsupported message type: %d.";
    private static final String OUT_OF_RANGE = "%s: %d: Out of range";


    private static final String BITS = " bits.";
    public static final String VERSION_NUMBER = "1.0.0";

    private StringTools() {
    }

    public static final String outOfRange(String name, Integer value){
        return String.format(OUT_OF_RANGE, name, value);
    }

    public static final String unsupportedMessage(Integer type){
        return String.format(UNSUPPORTED_MESSAGE, type);
    }

    public static final String tooShortToDetermine(String bitString){
        return String.format(TOO_SHORT_TO_DETERMINE_TYPE, bitString.length());
    }

    public static final String bitMessageIllegal(int messageType, String reason, String bitString){
        return bitMessage(MESSAGE_TYPE + messageType +":"+ reason, bitString);
    }

    public static final String bitMessage(String reason, String bitString){
        return reason + bitString.length() + BITS;
    }

    public static String version() {
        return  "\n" + "AIVDMDecoder v" + VERSION_NUMBER;
    }

}
