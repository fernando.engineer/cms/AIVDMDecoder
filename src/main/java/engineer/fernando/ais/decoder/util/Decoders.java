/*
 * Decoders
 */

package engineer.fernando.ais.decoder.util;

import java.util.Map;
import java.util.TreeMap;
import java.util.function.*;

public class Decoders {

    private Decoders() {
    }
    public static final int MIN_MSG_LEN = 6;

    private static final Map<Integer, String> SIX_BIT_ASCII = new TreeMap<>();

    static {
        SIX_BIT_ASCII.put(0, "@"); // 0
        SIX_BIT_ASCII.put(1, "A"); // 1
        SIX_BIT_ASCII.put(2, "B"); // 2
        SIX_BIT_ASCII.put(3, "C"); // 3
        SIX_BIT_ASCII.put(4, "D"); // 4
        SIX_BIT_ASCII.put(5, "E"); // 5
        SIX_BIT_ASCII.put(6, "F"); // 6
        SIX_BIT_ASCII.put(7, "G"); // 7
        SIX_BIT_ASCII.put(8, "H"); // 8
        SIX_BIT_ASCII.put(9, "I"); // 9
        SIX_BIT_ASCII.put(10, "J"); // 10
        SIX_BIT_ASCII.put(11, "K"); // 11
        SIX_BIT_ASCII.put(12, "L"); // 12
        SIX_BIT_ASCII.put(13, "M"); // 13
        SIX_BIT_ASCII.put(14, "N"); // 14
        SIX_BIT_ASCII.put(15, "O"); // 15
        SIX_BIT_ASCII.put(16, "P"); // 16
        SIX_BIT_ASCII.put(17, "Q"); // 17
        SIX_BIT_ASCII.put(18, "R"); // 18
        SIX_BIT_ASCII.put(19, "S"); // 19
        SIX_BIT_ASCII.put(20, "T"); // 20
        SIX_BIT_ASCII.put(21, "U"); // 21
        SIX_BIT_ASCII.put(22, "V"); // 22
        SIX_BIT_ASCII.put(23, "W"); // 23
        SIX_BIT_ASCII.put(24, "X"); // 24
        SIX_BIT_ASCII.put(25, "Y"); // 25
        SIX_BIT_ASCII.put(26, "Z"); // 26
        SIX_BIT_ASCII.put(27, "["); // 27
        SIX_BIT_ASCII.put(28, "\\"); // 28
        SIX_BIT_ASCII.put(29, "]"); // 29
        SIX_BIT_ASCII.put(30, "^"); // 30
        SIX_BIT_ASCII.put(31, "_"); // 31
        SIX_BIT_ASCII.put(32, " "); // 32
        SIX_BIT_ASCII.put(33, "!"); // 33
        SIX_BIT_ASCII.put(34, "\""); // 34
        SIX_BIT_ASCII.put(35, "#"); // 35
        SIX_BIT_ASCII.put(36, "$"); // 36
        SIX_BIT_ASCII.put(37, "%"); // 37
        SIX_BIT_ASCII.put(38, "&"); // 38
        SIX_BIT_ASCII.put(39, "'"); // 39
        SIX_BIT_ASCII.put(40, "("); // 40
        SIX_BIT_ASCII.put(41, ")"); // 41
        SIX_BIT_ASCII.put(42, "*"); // 42
        SIX_BIT_ASCII.put(43, "+"); // 43
        SIX_BIT_ASCII.put(44, ","); // 44
        SIX_BIT_ASCII.put(45, "-"); // 45
        SIX_BIT_ASCII.put(46, "."); // 46
        SIX_BIT_ASCII.put(47, "/"); // 47
        SIX_BIT_ASCII.put(48, "0"); // 48
        SIX_BIT_ASCII.put(49, "1"); // 49
        SIX_BIT_ASCII.put(50, "2"); // 50
        SIX_BIT_ASCII.put(51, "3"); // 51
        SIX_BIT_ASCII.put(52, "4"); // 52
        SIX_BIT_ASCII.put(53, "5"); // 53
        SIX_BIT_ASCII.put(54, "6"); // 54
        SIX_BIT_ASCII.put(55, "7"); // 55
        SIX_BIT_ASCII.put(56, "8"); // 56
        SIX_BIT_ASCII.put(57, "9"); // 57
        SIX_BIT_ASCII.put(58, ":"); // 58
        SIX_BIT_ASCII.put(59, ";"); // 59
        SIX_BIT_ASCII.put(60, "<"); // 60
        SIX_BIT_ASCII.put(61, "="); // 61
        SIX_BIT_ASCII.put(62, ">"); // 62
        SIX_BIT_ASCII.put(63, "?"); // 63
    }


    public static Map<String, String> getCharToSixBit() {
        return charToSixBit;
    }

    private static final Map<String, String> charToSixBit = new TreeMap<>();

    static {
        charToSixBit.put("0", "000000"); // 0
        charToSixBit.put("1", "000001"); // 1
        charToSixBit.put("2", "000010"); // 2
        charToSixBit.put("3", "000011"); // 3
        charToSixBit.put("4", "000100"); // 4
        charToSixBit.put("5", "000101"); // 5
        charToSixBit.put("6", "000110"); // 6
        charToSixBit.put("7", "000111"); // 7
        charToSixBit.put("8", "001000"); // 8
        charToSixBit.put("9", "001001"); // 9
        charToSixBit.put(":", "001010"); // 10
        charToSixBit.put(";", "001011"); // 11
        charToSixBit.put("<", "001100"); // 12
        charToSixBit.put("=", "001101"); // 13
        charToSixBit.put(">", "001110"); // 14
        charToSixBit.put("?", "001111"); // 15
        charToSixBit.put("@", "010000"); // 16
        charToSixBit.put("A", "010001"); // 17
        charToSixBit.put("B", "010010"); // 18
        charToSixBit.put("C", "010011"); // 19
        charToSixBit.put("D", "010100"); // 20
        charToSixBit.put("E", "010101"); // 21
        charToSixBit.put("F", "010110"); // 22
        charToSixBit.put("G", "010111"); // 23
        charToSixBit.put("H", "011000"); // 24
        charToSixBit.put("I", "011001"); // 25
        charToSixBit.put("J", "011010"); // 26
        charToSixBit.put("K", "011011"); // 27
        charToSixBit.put("L", "011100"); // 28
        charToSixBit.put("M", "011101"); // 29
        charToSixBit.put("N", "011110"); // 30
        charToSixBit.put("O", "011111"); // 31
        charToSixBit.put("P", "100000"); // 32
        charToSixBit.put("Q", "100001"); // 33
        charToSixBit.put("R", "100010"); // 34
        charToSixBit.put("S", "100011"); // 35
        charToSixBit.put("T", "100100"); // 36
        charToSixBit.put("U", "100101"); // 37
        charToSixBit.put("V", "100110"); // 38
        charToSixBit.put("W", "100111"); // 39
        charToSixBit.put("`", "101000"); // 40
        charToSixBit.put("a", "101001"); // 41
        charToSixBit.put("b", "101010"); // 42
        charToSixBit.put("c", "101011"); // 43
        charToSixBit.put("d", "101100"); // 44
        charToSixBit.put("e", "101101"); // 45
        charToSixBit.put("f", "101110"); // 46
        charToSixBit.put("g", "101111"); // 47
        charToSixBit.put("h", "110000"); // 48
        charToSixBit.put("i", "110001"); // 49
        charToSixBit.put("j", "110010"); // 50
        charToSixBit.put("k", "110011"); // 51
        charToSixBit.put("l", "110100"); // 52
        charToSixBit.put("m", "110101"); // 53
        charToSixBit.put("n", "110110"); // 54
        charToSixBit.put("o", "110111"); // 55
        charToSixBit.put("p", "111000"); // 56
        charToSixBit.put("q", "111001"); // 57
        charToSixBit.put("r", "111010"); // 58
        charToSixBit.put("s", "111011"); // 59
        charToSixBit.put("t", "111100"); // 60
        charToSixBit.put("u", "111101"); // 61
        charToSixBit.put("v", "111110"); // 62
        charToSixBit.put("w", "111111"); // 63
    }

    private static final Boolean STRIP_ALPHA_SIGNS = true;

    public static final ToIntFunction<String> INTEGER_DECODER = (Decoders::decodeStringToInteger);

    private static int decodeStringToInteger(String bitString) {
        Integer value;
        var signBit = bitString.substring(0, 1);
        var valueBits = bitString.substring(1);
        if ("0".equals(signBit))
            value = UNSIGNED_INTEGER_DECODER.applyAsInt(valueBits);
        else {
            valueBits = valueBits.replace("0", "x");
            valueBits = valueBits.replace("1", "0");
            valueBits = valueBits.replace("x", "1");
            value = -1 - UNSIGNED_INTEGER_DECODER.applyAsInt(valueBits);
        }
        return value;
    }

    public static final Function<String, Float> FLOAT_DECODER = bitString -> Float.valueOf(INTEGER_DECODER.applyAsInt(bitString));

    public static final Predicate<String> BOOLEAN_DECODER = bitString -> "1".equals(bitString.substring(0, 1));

    public static final ToIntFunction<String> UNSIGNED_INTEGER_DECODER = bitString -> Integer.parseUnsignedInt(bitString, 2);

    public static final ToLongFunction<String> UNSIGNED_LONG_DECODER = bitString -> Long.parseUnsignedLong(bitString, 2);

    public static final Function<String, Float> UNSIGNED_FLOAT_DECODER = bitString -> Float.valueOf(UNSIGNED_INTEGER_DECODER.applyAsInt(bitString));

    public static final UnaryOperator<String> TIME_DECODER = bitString -> {
        var month = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(0, 4));
        var day = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(4, 9));
        var hour = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(9, 14));
        var minute = UNSIGNED_INTEGER_DECODER.applyAsInt(bitString.substring(14, 20));

        String monthAsString = month<10 ? "0"+month : ""+month;
        String dayAsString = day<10 ? "0"+day : ""+day;
        String hourAsString = hour<10 ? "0"+hour : ""+hour;
        String minuteAsString = minute<10 ? "0"+minute : ""+minute;

        return dayAsString + "-" + monthAsString + " " + hourAsString + ":" + minuteAsString;
    };

    public static final UnaryOperator<String> STRING_DECODER = bitString -> {
            var stringBuffer = new StringBuffer();
            var remainingBits = bitString;
            while (remainingBits.length() >= 6) {
                var b = remainingBits.substring(0, 6);
                remainingBits = remainingBits.substring(6);
                var i = UNSIGNED_INTEGER_DECODER.applyAsInt(b);
                String c = SIX_BIT_ASCII.get(i);
                stringBuffer.append(c);
            }
            var str = stringBuffer.toString();
            if (Boolean.TRUE.equals(STRIP_ALPHA_SIGNS)) {
                str = str.replace("@", " ").trim();
            }
            return str;
    };

    public static final UnaryOperator<String> BIT_DECODER = bitString -> bitString;

}