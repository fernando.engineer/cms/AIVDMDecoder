package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.PositionReportClassAScheduled;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.types.NavigationStatus;
import engineer.fernando.ais.decoder.messages.types.SOTDMACommunicationState;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;

public class MsgType1_PositionReportClassAScheduledTest extends TestCase {

    public void testFullParse_SOTDMACommunicationState_slotTimeout3() {
        String demoNmeaStrings = "!AIVDM,1,1,,B,19NS7Sp02wo?HETKA2K6mUM20<L=,0*27";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        assertEquals("msgid", AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED, supermsg.getMessageType());
        assertTrue(supermsg instanceof PositionReportClassAScheduled);
        PositionReportClassAScheduled msg = (PositionReportClassAScheduled) supermsg;
        assertEquals("userid", 636012431, msg.getSourceMmsi().getMMSI().intValue());
        assertEquals("repeat", 0, msg.getRepeatIndicator().intValue());
        assertEquals("nav_status", NavigationStatus.UNDERWAY_SAILING, msg.getNavigationStatus());
        assertEquals("rot", 0, msg.getRateOfTurn().intValue());
        assertEquals("sog", 19.1, msg.getSpeedOverGround(),0.1);
        assertTrue("pos_acc",msg.getPositionAccuracy());
        assertEquals("longitude", -122.469253, msg.getLongitude(),0.000001);
        assertEquals("latitude", 47.651165, msg.getLatitude(),0.000001);
        assertEquals("Curse over Ground", 175.0, msg.getCourseOverGround(), 0.01);
        assertEquals("true_heading", 174, msg.getTrueHeading().intValue());
        assertEquals("utc_sec", 33, msg.getSecond().intValue());
        assertTrue("Communication State" , msg.getCommunicationState() instanceof SOTDMACommunicationState);
        SOTDMACommunicationState commState = (SOTDMACommunicationState) msg.getCommunicationState();
        System.out.println("Communication State details: " + commState);
        assertEquals("Slot Timeout", 3, commState.getSlotTimeout().intValue());
        assertEquals("# of Received Stations", 1805, commState.getNumberOfReceivedStations().intValue());
        assertEquals("Slot Number", null, commState.getSlotNumber());
        assertEquals("UTH Hour", null, commState.getUtcHour());
        assertEquals("UTH Minute", null, commState.getUtcMinute());
        assertEquals("Slot OffSet", null, commState.getSlotOffset());
        assertEquals("Sync State Code", 0, commState.getSyncState().getCode().intValue());
        assertEquals("Sync State Value", "UTC_DIRECT", commState.getSyncState().getValue());
        assertFalse("Raim Flag", msg.getRaimFlag());
    }

    public void testParse_SOTDMACommunicationState_slotTimeout2() {
        String demoNmeaStrings = "!AIVDM,1,1,,A,152SGj001to?TvlEg4`H?6UL08Jo,0*36";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        PositionReportClassAScheduled msg = (PositionReportClassAScheduled) supermsg;
        SOTDMACommunicationState commState = (SOTDMACommunicationState) msg.getCommunicationState();
        System.out.println("Communication State details: " + commState);
        assertEquals("Slot Timeout", 2, commState.getSlotTimeout().intValue());
        assertEquals("# of Received Stations", null, commState.getNumberOfReceivedStations());
        assertEquals("Slot Number", 1719, commState.getSlotNumber().intValue());
        assertEquals("UTH Hour", null, commState.getUtcHour());
        assertEquals("UTH Minute", null, commState.getUtcMinute());
        assertEquals("Slot OffSet", null, commState.getSlotOffset());
    }

    public void testParse_SOTDMACommunicationState_slotTimeout1() {
        String demoNmeaStrings = "!AIVDM,1,1,,A,13u?etPv2;0n:dDPwUM1U1Cb069D,0*23";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        PositionReportClassAScheduled msg = (PositionReportClassAScheduled) supermsg;
        SOTDMACommunicationState commState = (SOTDMACommunicationState) msg.getCommunicationState();
        System.out.println("Communication State details: " + commState);
        assertEquals("Slot Timeout", 1, commState.getSlotTimeout().intValue());
        assertEquals("# of Received Stations", null, commState.getNumberOfReceivedStations());
        assertEquals("Slot Number", null, commState.getSlotNumber());
        assertEquals("UTH Hour", 17, commState.getUtcHour().intValue());
        assertEquals("UTH Minute", 21, commState.getUtcMinute().intValue());
        assertEquals("Slot OffSet", null, commState.getSlotOffset());
    }

    public void testParse_SOTDMACommunicationState_slotTimeout0() {
        String demoNmeaStrings = "!AIVDM,1,1,,A,13ukmN7@0<0pRcHPTkn4P33f0000,0*58";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        PositionReportClassAScheduled msg = (PositionReportClassAScheduled) supermsg;
        SOTDMACommunicationState commState = (SOTDMACommunicationState) msg.getCommunicationState();
        System.out.println("Communication State details: " + commState);
        assertEquals("Slot Timeout", 0, commState.getSlotTimeout().intValue());
        assertEquals("# of Received Stations", null, commState.getNumberOfReceivedStations());
        assertEquals("Slot Number", null, commState.getSlotNumber());
        assertEquals("UTH Hour", null, commState.getUtcHour());
        assertEquals("UTH Minute", null, commState.getUtcMinute());
        assertEquals("Slot OffSet", 0, commState.getSlotOffset().intValue());
    }
}
