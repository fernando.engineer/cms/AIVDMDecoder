package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.SafetyRelatedBroadcastMessage;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;

public class MsgType14_SafetyRelatedBroadcastMessageTest extends TestCase {

    public void testFullParse() {
        String demoNmeaStrings = "!AIVDM,1,1,,B,>>M4fWA<59B1@E=@,0*17";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings));
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        assertEquals("msgid", AISMessageType.SAFETY_RELATED_BROADCAST_MESSAGE, supermsg.getMessageType());
        assertTrue(supermsg instanceof SafetyRelatedBroadcastMessage);
        SafetyRelatedBroadcastMessage msg = (SafetyRelatedBroadcastMessage) supermsg;
        assertEquals("userid", 970010269, msg.getSourceMmsi().getMMSI().intValue());
        assertEquals("spare", 0, msg.getSpare().intValue());
        assertEquals("text", "SART TEST", msg.getText());
        assertEquals("repeat", 0, msg.getRepeatIndicator().intValue());
    }
}
