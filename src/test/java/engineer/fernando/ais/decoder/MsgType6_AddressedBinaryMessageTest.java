package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.AddressedBinaryMessage;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class MsgType6_AddressedBinaryMessageTest extends TestCase {

    private static final Logger LOG = LogManager.getLogger(MsgType6_AddressedBinaryMessageTest.class);
    private static final String NMEA_STRING_STREAM = "!AIVDM,1,1,4,B,6>jR0600V:C0>da4P106P00,2*02";

    public void testAddressedBinaryMessage() {
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(NMEA_STRING_STREAM));
        LOG.info("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);
        assertEquals("msgid", AISMessageType.ADDRESSED_BINARY_MESSAGE, supermsg.getMessageType());
        assertTrue(supermsg instanceof AddressedBinaryMessage);
        AddressedBinaryMessage msg = (AddressedBinaryMessage) supermsg;
        assertEquals("userid", 992509976, msg.getSourceMmsi().getMMSI().intValue());
    }

}
