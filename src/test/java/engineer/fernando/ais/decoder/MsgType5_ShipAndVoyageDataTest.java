package engineer.fernando.ais.decoder;

import engineer.fernando.ais.decoder.messages.ShipAndVoyageData;
import engineer.fernando.ais.decoder.messages.AISMessage;
import engineer.fernando.ais.decoder.messages.types.AISMessageType;
import engineer.fernando.ais.decoder.nmea.NMEAMessage;
import junit.framework.TestCase;

public class MsgType5_ShipAndVoyageDataTest extends TestCase {

    public void testParse() {
        String demoNmeaStrings1 = "!AIVDM,2,1,9,B,59NRrG@25UtPDhiOH00q21<D@<v3O3H00000,0*79";
        String demoNmeaStrings2 = "!AIVDM,2,2,9,B,001J2PtDe55P0wDSkPhA3l`0Pp000000000,2*5F";
        AISMessage supermsg = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings1), NMEAMessage.fromString(demoNmeaStrings2) );
        System.out.println("Received AIS message: MMSI[" + supermsg.getSourceMmsi().getMMSI() + "]: " + supermsg);

        assertEquals("msgid", AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA, supermsg.getMessageType());
        assertTrue(supermsg instanceof ShipAndVoyageData);
        ShipAndVoyageData msg = (ShipAndVoyageData) supermsg;
        assertEquals("userid", 636009053, msg.getSourceMmsi().getMMSI().intValue());

        String demoNmeaStrings3 = "!AIVDM,2,1,0,A,58wt8Ui`g??r21`7S=:22058<v05Htp000000015>8OA;0sk,0*7B";
        String demoNmeaStrings4 = "!AIVDM,2,2,0,A,eQ8823mDm3kP00000000000,2*5D";
        AISMessage supermsg2 = AISMessage.create(NMEAMessage.fromString(demoNmeaStrings3), NMEAMessage.fromString(demoNmeaStrings4) );
        System.out.println("Received AIS message: MMSI[" + supermsg2.getSourceMmsi().getMMSI() + "]: " + supermsg2);

        assertEquals("msgid", AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA, supermsg2.getMessageType());
        assertTrue(supermsg2 instanceof ShipAndVoyageData);
        ShipAndVoyageData msg2 = (ShipAndVoyageData) supermsg2;
        assertEquals("userid", 603916439, msg2.getSourceMmsi().getMMSI().intValue());

    }
}